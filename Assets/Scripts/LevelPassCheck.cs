﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelPassCheck : MonoBehaviour
{
    private GameObject player;
    private PlayerController playerScript;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name != "Level Editor")
        {
            player = GameObject.FindWithTag("Collector");
            playerScript = player.GetComponent<PlayerController>();
        }
    }

    //Check if game object reaches end of the path for a level, if the backmost part of the gripper exit the end trigger it means that it reaches.
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Border")
        {
            if(gameObject.tag == "Level Passed")
                playerScript.LevelPassed();
            else if(gameObject.tag == "Begin Level End")
                playerScript.BeginLevelEnd();
        }
    }
}