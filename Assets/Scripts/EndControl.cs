﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndControl : MonoBehaviour
{
    private GameObject player;
    private PlayerController playerScript;

    private void Start()
    {
        player = GameObject.FindWithTag("Collector");
        playerScript = player.GetComponent<PlayerController>();
    }

    //Check if game object reaches end of the path for a level, if the backmost part of the gripper exit the end trigger it means that it reaches.
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Border")
        {
            playerScript.OnChildTriggerExit(other);
        }
    }
}