﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    private Rigidbody rb;
    public bool inside = false; //Condition to check if collectable is in gripper.
    public bool moved = false; //Check if collectable is pushed to basket at the end of the level.
    public static int counter = 0; //Score i.e. number of collectable exists in gripper.

    void Start(){
        rb = GetComponent<Rigidbody>();
    }

    //Detect number of collectable brought at the end of the level. There is a hidden gameboject inside and checking by triggering a collectable with it.
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Inside")
        {
            inside = true;
            counter ++;
        }
    }

    //Check if collecatble gets out from collider, update score and condition variables accordingly.
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Inside")
        {
            counter--;
            if (counter < 0)
                counter = 0;
            inside = false;
        }
    }

    void Update()
    {
        //Preventing that collectables gets on each other and gets out with this way from gripper.
        if(transform.position.y > 1.15f){
            rb.MovePosition(new Vector3(transform.position.x, 1.15f, transform.position.z));
        }

        //Force collectables to the basket at the end of levels.
        if(PlayerController.endPath && !moved && inside){;
            rb.AddForce(0, 0, 10, ForceMode.Impulse);
            moved = true;
            inside = false;
        }
    }

    //Reset a collectable (sphere for now) means that it is stopped now.
    public void ResetCollectable()
    {
        moved = false;
        inside = false;
    }

}
