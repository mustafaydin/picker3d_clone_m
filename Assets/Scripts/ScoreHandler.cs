﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreHandler : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public static int currentScore;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Inside")
        {
            currentScore = int.Parse(scoreText.text);
        }
    }

    public void UpdateScore(string score)
    {
        scoreText.text = score;
    }

    public string GetScore()
    {
        return scoreText.text;
    }
}
