﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private float offset;

    //Set camera position according to picker.
    void Start()
    {
        offset = player.transform.position.z + 4.5f;
    }

    //Camera is following collector.
    void LateUpdate()
    {
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, player.transform.position.z - offset);
    }
}
