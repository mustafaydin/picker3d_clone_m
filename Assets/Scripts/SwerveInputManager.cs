﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwerveInputManager : MonoBehaviour
{
    private float lastPosX;
    private float deltaX;

    public float DeltaX()
    {
        return deltaX;
    }

    //Mouse input for horizantal movement.
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastPosX = Input.mousePosition.x;
            PlayerController.gameStart = true;
        }
        else if (Input.GetMouseButton(0))
        {
            deltaX = Input.mousePosition.x - lastPosX;
            lastPosX = Input.mousePosition.x;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            deltaX = 0;
        }
    }
}
