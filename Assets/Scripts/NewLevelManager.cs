﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System;

public class NewLevelManager : MonoBehaviour
{
    private string levelDataPath; //Level data path, Level Data folder in assets folder.

    //Path prefabs
    public GameObject tilePrefab;
    public GameObject basketPrefab;
    public GameObject endPhasePrefab;
    public GameObject endLevelPrefab;
    public GameObject barrierPrefab;
    public GameObject scorePrefab;

    //Collectable prefabs
    public GameObject spherePrefab;
    public GameObject cubePrefab;
    public GameObject cylinderPrefab;

    //Change screen according to game states.
    public GameObject screenHandler;
    private ScreenHandler screenScript;

    //Current and next level objects.
    public static List<List<GameObject>> levelTiles = new List<List<GameObject>>(); //List of tiles for current and next level.
    public static List<List<GameObject>> levelBaskets = new List<List<GameObject>>(); //List of basket for current and next level.
    public static List<List<GameObject>> levelBarriers = new List<List<GameObject>>(); //List of barriers for current and next level.
    public static List<List<Vector3>> allLevelCollectables = new List<List<Vector3>>(); //List of collectable object for each loaded levels to reset them when necessary.
    public static List<string[]> phaseRequirements = new List<string[]>(); //List of phase requirements for for current and next level.
    public static List<float> endPathPosZ = new List<float>(); //List of end path position to set reset position of picker.
    private static List<GameObject> levelParents = new List<GameObject>(); //All level parent objects to keep object related with a level together.

    public static int numOfLevel; //Number of level exists in level data folder.
    string[] allLevelFiles; //All level files in level data folder.

    //Return all level objects to object pool at the end of the level. Also, resetting properties of collectable objects. Then, destroy parent level object.
    public void ReturnAllLevelObjects()
    {
        GameObject currentLevel = levelParents[PlayerController.levelCounter - 1];
        foreach (Transform child in currentLevel.transform)
        {
            ObjectPool.SharedIntance.ReturnGameObject(child.gameObject);
            if(child.gameObject.tag == "Collectable")
                child.gameObject.GetComponent<CollectableController>().ResetCollectable();
        }
        currentLevel.transform.DetachChildren();
        Destroy(currentLevel);
    }

    //Set positions and parents of all level objects.
    void SetPosOfObject(GameObject obj, Vector3 pos, GameObject newParent)
    {
        obj.transform.position = pos;
        obj.transform.rotation = Quaternion.identity;
        obj.transform.SetParent(newParent.transform, false);
    }

    //Place path objects for a level.
    private void PlacePathObjects(LevelData loadLevel, GameObject levelParent)
    {
        float lastPosZ = PlayerPrefs.GetFloat("LastZ", 0f);
        endPathPosZ.Add(lastPosZ);
        List<GameObject> baskets = new List<GameObject>();
        List<GameObject> barriers = new List<GameObject>();
        List<GameObject> tiles = new List<GameObject>();
        phaseRequirements.Add(loadLevel.phaseRequirements);

        for (int i = 0; i < loadLevel.pathCoordinates.Count; i++)
        {
            string type = loadLevel.pathTypes[i];

            switch (type)
            {
                case "Tile":
                    GameObject tile = ObjectPool.SharedIntance.GetObject(tilePrefab);
                    Vector3 tilePos = new Vector3(loadLevel.pathCoordinates[i].x, loadLevel.pathCoordinates[i].y, loadLevel.pathCoordinates[i].z + lastPosZ);
                    SetPosOfObject(tile, tilePos, levelParent);
                    tiles.Add(tile);
                    break;
                case "Basket":
                    GameObject basket = ObjectPool.SharedIntance.GetObject(basketPrefab);
                    Vector3 basketPos = new Vector3(loadLevel.pathCoordinates[i].x, loadLevel.pathCoordinates[i].y, loadLevel.pathCoordinates[i].z + lastPosZ);
                    SetPosOfObject(basket, basketPos, levelParent);
                    baskets.Add(basket);

                    GameObject barrier = ObjectPool.SharedIntance.GetObject(barrierPrefab);
                    Vector3 barrierPos = new Vector3(-33, -22.7f, basket.transform.position.z + 12.7f);
                    SetPosOfObject(barrier, barrierPos, levelParent);
                    barriers.Add(barrier);

                    Vector3 endPhasePos = new Vector3(-4, 0, basket.transform.position.z - 2);
                    GameObject endPhase = ObjectPool.SharedIntance.GetObject(endPhasePrefab);
                    endPhase.transform.position = endPhasePos;
                    endPhase.transform.rotation = Quaternion.Euler(270, 0, 0);
                    endPhase.transform.parent = levelParent.transform;
                    break;
                case "End Level Path":
                    GameObject endLevelPath = ObjectPool.SharedIntance.GetObject(endLevelPrefab);
                    Vector3 endLevelPos = new Vector3(loadLevel.pathCoordinates[i].x, loadLevel.pathCoordinates[i].y, loadLevel.pathCoordinates[i].z + lastPosZ);
                    SetPosOfObject(endLevelPath, endLevelPos, levelParent);
                    PlayerPrefs.SetFloat("LastZ", endLevelPath.transform.position.z + 15.6f);
                    break;
            }
        }
        levelTiles.Add(tiles);
        levelBaskets.Add(baskets);
        levelBarriers.Add(barriers);

        //Place score objects at the end of the levels.
        for (int i = 0; i < loadLevel.allScores.Count; i++)
        {
            GameObject score = ObjectPool.SharedIntance.GetObject(scorePrefab);
            SetPosOfObject(score, loadLevel.allScoreCoordinates[i], levelParent);
            score.GetComponent<ScoreHandler>().UpdateScore(loadLevel.allScores[i].ToString());
        }
    }

    //Place all collectable objects for a level.
    private void PlaceCollectableObjects(LevelData loadLevel, GameObject levelParent)
    {
        List<Vector3> collectables = new List<Vector3>();
        float lastPosZ = PlayerPrefs.GetFloat("LastObjZ", 0f);
        for (int i = 0; i < loadLevel.objectCoordinates.Count; i++)
        {
            string type = loadLevel.objectTypes[i];
            Vector3 objPos = new Vector3(loadLevel.objectCoordinates[i].x, loadLevel.objectCoordinates[i].y, loadLevel.objectCoordinates[i].z + lastPosZ);

            switch (type)
            {
                case "Sphere":
                    GameObject sphere = ObjectPool.SharedIntance.GetObject(spherePrefab);
                    SetPosOfObject(sphere, objPos, levelParent);
                    collectables.Add(sphere.transform.position);
                    break;
                case "Cube":
                    GameObject cube = ObjectPool.SharedIntance.GetObject(cubePrefab);
                    SetPosOfObject(cube, objPos, levelParent);
                    collectables.Add(cube.transform.position);
                    break;
                case "Cylinder":
                    GameObject cylinder = ObjectPool.SharedIntance.GetObject(cylinderPrefab);
                    SetPosOfObject(cylinder, objPos, levelParent);
                    collectables.Add(cylinder.transform.position);
                    break;
            }
        }
        allLevelCollectables.Add(collectables);
        PlayerPrefs.SetFloat("LastObjZ", PlayerPrefs.GetFloat("LastZ", 0));
    }

    //Load specific level by using its index.
    public void LoadSpecificLevel(int levelInd)
    {
        LevelData loadLevel = new LevelData();
        string json = File.ReadAllText(allLevelFiles[levelInd]);
        JsonUtility.FromJsonOverwrite(json, loadLevel);

        GameObject level = new GameObject("Level " + levelInd.ToString());
        PlacePathObjects(loadLevel, level);
        PlaceCollectableObjects(loadLevel, level);
        levelParents.Add(level);
    }

    //Load last 2 level at start, set first basket accordingly and set phase progress bar according to level data.
    void Start()
    {
        int currentLevelIndex = PlayerPrefs.GetInt("CurrentLevel", 0);
        int nextLevelIndex = PlayerPrefs.GetInt("NextLevel", 1);
        levelDataPath = Application.dataPath + "/Level Data";
        allLevelFiles = Directory.GetFiles(levelDataPath, "*.json");
        numOfLevel = allLevelFiles.Length;

        LoadSpecificLevel(currentLevelIndex);
        LoadSpecificLevel(nextLevelIndex);

        //Set score text on first basket.
        List<GameObject> baskets = NewLevelManager.levelBaskets[PlayerController.levelCounter];
        string[] requirements = NewLevelManager.phaseRequirements[PlayerController.levelCounter];
        int.TryParse(requirements[PlayerController.currentPhase], out int levelReq);
        screenScript.SetScoreOnBasket(levelReq, baskets[PlayerController.currentPhase]);

        //Set phase progress bar according to number of baskets i.e. phase.
        screenScript.SetStartPhaseProgress(baskets.Count - 1, 0);
    }

    //Set Z position 0 at the start of the game because game turns back Z=0 when user quit the game.
    private void Awake()
    {
        screenScript = screenHandler.GetComponent<ScreenHandler>();
        PlayerPrefs.SetFloat("LastObjZ", 0f);
        PlayerPrefs.SetFloat("LastZ", 0f);
    }

    //Reset position of collectable objects for a level when user fail.
    public void ResetCollectableObjects()
    {
        GameObject currentLevelObjects = levelParents[PlayerController.levelCounter];
        List<Vector3> currentLevelCollectables = allLevelCollectables[PlayerController.levelCounter];
        int resetCounter = 0;
        foreach (Transform child in currentLevelObjects.transform)
        {
            if (child.tag == "Collectable")
            {
                child.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                child.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                child.transform.position = currentLevelCollectables[resetCounter];
                child.transform.rotation = Quaternion.identity;
                resetCounter++;
            }
        }
    }

    //Clear stored previous level data.
    public void ClearPassedLevelData(int ind)
    {
        levelTiles.RemoveAt(ind);
        levelBaskets.RemoveAt(ind);
        levelBarriers.RemoveAt(ind);
        allLevelCollectables.RemoveAt(ind);
        phaseRequirements.RemoveAt(ind);
        endPathPosZ.RemoveAt(ind);
        levelParents.RemoveAt(ind);
        PlayerController.levelCounter--;
    }
}
