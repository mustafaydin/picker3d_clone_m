﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelEditorManager : MonoBehaviour
{
    public Button freeLockButton;
    public Button moveObjectButton;

    public static bool freeLookEnabled = true;

    public GameObject tilePrefab;
    public GameObject basketPrefab;
    public GameObject endLevelPrefab;
    private Vector3 tilePosition;
    private static Stack<GameObject> pathObjects = new Stack<GameObject>();

    private static List<GameObject> collectableObjects = new List<GameObject>();
    public float movementSpeed = 3f;
    public float fastMovementSpeed = 30f;
    private int objCounter = 0;
    public Text selectedObjText;
    public GameObject spherePrefab;
    public GameObject scorePrefab;
    public GameObject cubePrefab;
    public GameObject cylinderPrefab;
    private Transform selectedObj;
    private Vector3 objPosition;

    private static List<GameObject> scoreObjects = new List<GameObject>();
    private Vector3 scorePosition;
    public InputField scoreTextField;
    private int scoreCounter = 0;

    public InputField levelName;
    public Text levelStatus;
    public static bool levelFieldActive = false;
    private string levelDataPath;

    public InputField phaseRequirements;

    private void Start()
    {
        levelDataPath = Application.dataPath + "/Level Editor Data";
        tilePosition = new Vector3(0, 0, 0);
        objPosition = new Vector3(-4, 1.13f, 0);
        scorePosition = objPosition;
        scorePosition.y = 1f;
    }

    ///////MODE SELECTION///////

    //Free look enabled.
    public void FreeLookSelected()
    {
        freeLookEnabled = true;
        freeLockButton.GetComponent<Image>().color = Color.green;
        moveObjectButton.GetComponent<Image>().color = Color.red;
    }

    //Move game object, free look disabled.
    public void MoveObjectSelected()
    {
        freeLookEnabled = false;
        freeLockButton.GetComponent<Image>().color = Color.red;
        moveObjectButton.GetComponent<Image>().color = Color.green;
    }

    ///////END - MODE SELECTION///////

    ///////PATH///////

    //Increase or decrease tile position to keep position for adding new one.
    public void UpdateTilePosition(bool added)
    {
        if(added)
            tilePosition = new Vector3(tilePosition.x, tilePosition.y, tilePosition.z + 8f);
        else
            tilePosition = new Vector3(tilePosition.x, tilePosition.y, tilePosition.z - 8f);
    }

    //Add tile prefab to the tile position.
    public void AddTile()
    {
        GameObject tile = Instantiate(tilePrefab, tilePosition, Quaternion.identity);
        UpdateTilePosition(true);
        pathObjects.Push(tile);
    }

    //Add basket to the end of the level.
    public void AddBasket()
    {
        GameObject basket = Instantiate(basketPrefab, new Vector3(tilePosition.x, -1f, tilePosition.z), Quaternion.identity);
        UpdateTilePosition(true);
        pathObjects.Push(basket);
    }

    public void AddEndLevel()
    {
        GameObject endLevel = Instantiate(endLevelPrefab, new Vector3(tilePosition.x -3.96f, 0.94f, tilePosition.z + 15.6f), Quaternion.identity);
        UpdateTilePosition(true);
        pathObjects.Push(endLevel);
        scorePosition = endLevel.transform.position;
        scorePosition.y = 1f;
        scorePosition.z = scorePosition.z - 13f;
    }

    public void AddScore()
    {
        GameObject score = Instantiate(scorePrefab, scorePosition, Quaternion.identity);
        score.name = "Sc" + scoreCounter.ToString();
        string inputScore = scoreTextField.text;
        score.GetComponent<ScoreHandler>().UpdateScore(inputScore);
        scoreObjects.Add(score);
        scoreCounter++;
        scorePosition.z += 1.6f;
    }

    //Delete last path object, using stack's LIFO property.
    public void DeleteLastPathObject()
    {
        if (pathObjects.Count > 0)
        {
            Destroy(pathObjects.Pop());
            UpdateTilePosition(false);
        }
    }
    ///////END - PATH///////

    ///////COLLECTABLE OBJECTS///////

    //Add sphere to start position for object placing via button.
    public void AddSphere()
    {
        objCounter++;
        GameObject sphere = Instantiate(spherePrefab, objPosition, Quaternion.identity);
        sphere.name = "Sp" + objCounter.ToString();
        sphere.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        collectableObjects.Add(sphere);
    }

    //Add cube to start position for object placing via button.
    public void AddCube()
    {
        objCounter++;
        GameObject cube = Instantiate(cubePrefab, objPosition, Quaternion.identity);
        cube.name = "Cb" + objCounter.ToString();
        cube.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        collectableObjects.Add(cube);
    }

    //Add cylinder to start position for object placing via button.
    public void AddCylinder()
    {
        objCounter++;
        GameObject cylinder = Instantiate(cylinderPrefab, objPosition, Quaternion.identity);
        cylinder.name = "Cy" + objCounter.ToString();
        cylinder.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        collectableObjects.Add(cylinder);
    }

    //Move selected objects via keyboard control.
    public void MoveObjects()
    {
        var fastMode = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        var movementSpeed = fastMode ? this.fastMovementSpeed : this.movementSpeed;

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            selectedObj.position = selectedObj.position + (-selectedObj.right * movementSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            selectedObj.position = selectedObj.position + (selectedObj.right * movementSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            selectedObj.position = selectedObj.position + (selectedObj.forward * movementSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            selectedObj.position = selectedObj.position + (-selectedObj.forward * movementSpeed * Time.deltaTime);
        }
    }

    //Delete selected object if it is a collectable game object.
    public void DeleteSelectedObject()
    {
        objCounter--;
        if (selectedObj.tag == "Collectable")
        {
            collectableObjects.Remove(selectedObj.gameObject);
            Destroy(selectedObj.gameObject);
        }

        if(selectedObj.tag == "Score Floor")
        {
            scoreObjects.Remove(selectedObj.gameObject);
            scoreCounter--;
            Destroy(selectedObj.gameObject);
        }
    }
    ///////END - OBJECTS///////

    /////// SAVE and LOAD LEVEL ///////

    //Check whether input field is active or not to prevent moving a gameobject when typing a level name.
    public void LevelNameChanging()
    {
        levelFieldActive = true;
    }
    public void LevelNameEntered()
    {
        levelFieldActive = false;
    }

    //Create level  details.
    private LevelData GetLevelObject(string lvlName)
    {
        LevelData levelData = new LevelData();
        levelData.levelName = lvlName;

        List<string> pathTypes = new List<string>();
        List<Vector3> pathCoords = new List<Vector3>();
        foreach (GameObject item in pathObjects)
        {
            pathCoords.Add(item.transform.position);

            if(item.tag == "Tile")
            {
                pathTypes.Add("Tile");
            }

            if(item.tag == "Basket")
            {
                pathTypes.Add("Basket");
            }

            if(item.tag == "End Level Path")
            {
                pathTypes.Add("End Level Path");
            }
        }
        pathTypes.Reverse();
        pathCoords.Reverse();
        levelData.pathTypes = pathTypes;
        levelData.pathCoordinates = pathCoords;

        List<Vector3> scoreCoords = new List<Vector3>();
        List<int> allScores = new List<int>();
        foreach (GameObject item in scoreObjects)
        {
            allScores.Add(int.Parse(item.GetComponent<ScoreHandler>().GetScore()));
            scoreCoords.Add(item.transform.position);
        }
        levelData.allScoreCoordinates = scoreCoords;
        levelData.allScores = allScores;

        List<Vector3> objCoords = new List<Vector3>();
        List<string> objTypes = new List<string>();
        foreach (GameObject item in collectableObjects)
        {
            objCoords.Add(item.transform.position);

            if (item.name.StartsWith("Sp"))
            {
                objTypes.Add("Sphere");
            }
            else if (item.name.StartsWith("Cb"))
            {
                objTypes.Add("Cube");
            }
            else if (item.name.StartsWith("Cy"))
            {
                objTypes.Add("Cylinder");
            }
        }

        levelData.objectCoordinates = objCoords;
        levelData.objectTypes = objTypes;
        levelData.phaseRequirements = phaseRequirements.text.Split(',');

        return levelData;
    }

    //Save level details in a JSON file.
    public void SaveLevel()
    {
        string lvlName = levelName.text;
        if (lvlName != "")
        {
            LevelData newLevel = GetLevelObject(lvlName);
            string json = JsonUtility.ToJson(newLevel, true);
            System.IO.File.WriteAllText(levelDataPath + "/" + lvlName + ".json", json);
            levelStatus.text = "Saved: " + lvlName;
        }
        else
        {
            levelStatus.text = "Write A Level Name!";
        }
    }

    //Place level objects according to details from stored data.
    private void PlaceLevelObjects(LevelData loadLevel)
    {
        for (int i = 0; i < loadLevel.pathCoordinates.Count; i++)
        {
            UpdateTilePosition(true);
            if (loadLevel.pathTypes[i] == "Basket")
            {
                GameObject basket = Instantiate(basketPrefab, loadLevel.pathCoordinates[i], Quaternion.identity);
                pathObjects.Push(basket);
            }
            else if (loadLevel.pathTypes[i] == "End Level Path")
            {
                GameObject endLevel = Instantiate(endLevelPrefab, loadLevel.pathCoordinates[i], Quaternion.identity);
                pathObjects.Push(endLevel);
                scorePosition = endLevel.transform.position;
                scorePosition.y = 1f;
                scorePosition.z = scorePosition.z - 13f;
            }
            else
            {
                GameObject tile = Instantiate(tilePrefab, loadLevel.pathCoordinates[i], Quaternion.identity);
                pathObjects.Push(tile);
            }
        }

        for (int i = 0; i < loadLevel.objectCoordinates.Count; i++)
        {
            if(loadLevel.objectTypes[i] == "Sphere")
            {
                objCounter++;
                GameObject sphere = Instantiate(spherePrefab, loadLevel.objectCoordinates[i], Quaternion.identity);
                sphere.name = "Sp" + objCounter.ToString();
                sphere.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
                collectableObjects.Add(sphere);
            }

            if (loadLevel.objectTypes[i] == "Cube")
            {
                objCounter++;
                GameObject cube = Instantiate(cubePrefab, loadLevel.objectCoordinates[i], Quaternion.identity);
                cube.name = "Cb" + objCounter.ToString();
                cube.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
                collectableObjects.Add(cube);
            }

            if (loadLevel.objectTypes[i] == "Cylinder")
            {
                objCounter++;
                GameObject cylinder = Instantiate(cylinderPrefab, loadLevel.objectCoordinates[i], Quaternion.identity);
                cylinder.name = "Cy" + objCounter.ToString();
                cylinder.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
                collectableObjects.Add(cylinder);
            }
        }

        for (int i = 0; i < loadLevel.allScoreCoordinates.Count; i++)
        {
            scoreCounter++;
            GameObject score = Instantiate(scorePrefab, loadLevel.allScoreCoordinates[i], Quaternion.identity);
            score.name = "Sc" + scoreCounter.ToString();
            scoreObjects.Add(score);
        }
    }

    //Load level by reading saved JSON file.
    public void LoadLevel()
    {
        string lvlName = levelName.text;
        string filePath = levelDataPath + "/" + lvlName + ".json";
        if (System.IO.File.Exists(filePath))
        {
            CloseOpenLevel();
            LevelData loadLevel = new LevelData();
            string json = System.IO.File.ReadAllText(filePath);
            JsonUtility.FromJsonOverwrite(json, loadLevel);
            PlaceLevelObjects(loadLevel);
            levelStatus.text = "Loaded: " + lvlName;
        }
        else
        {
            levelStatus.text = "Could not found file!";
        }
    }

    //Close current level in editor before loading new one.
    void CloseOpenLevel()
    {
        //Delete path.
        foreach (GameObject item in pathObjects)
        {
            Destroy(item);
            UpdateTilePosition(false);
        }
        pathObjects.Clear();

        //Delete objects.
        foreach(GameObject item in collectableObjects)
        {
            Destroy(item);
        }
        collectableObjects.Clear();
        objCounter = 0;
    }

    /////// END - SAVE and LOAD LEVEL ///////


    private void Update()
    {
        //Select game object to delete and move.
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                selectedObj = hit.collider.gameObject.transform;
                selectedObjText.text = "Selected Obj: " + selectedObj.name;
            }
        }

        //Move object enabled if it is a collectable object, move mode eneabled and level field is not active.
        if (selectedObj != null)
        {
            if ((selectedObj.name.StartsWith("Sp") || selectedObj.name.StartsWith("Cb") || selectedObj.name.StartsWith("Cy") || selectedObj.name.StartsWith("Sc")) && !freeLookEnabled && !levelFieldActive)
            {
                MoveObjects();
            }
        }
    }
}
