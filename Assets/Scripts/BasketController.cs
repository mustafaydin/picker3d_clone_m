using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BasketController : MonoBehaviour
{
    public GameObject frontBarrier;
    public GameObject backBarrier;

    //Stop the collected object and hide them when passed level.
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Collectable")
        {
            other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            other.gameObject.transform.rotation = Quaternion.identity;
            if (PlayerController.passed)
            {
                StartCoroutine(CollectableAction(other.gameObject));
            }
        }

        //Collector and basket should not collide because basket can block movement of picker when it goes above at the end of a phase.
        if(other.gameObject.tag == "Collector")
        {
            Physics.IgnoreCollision(other.collider, GetComponent<BoxCollider>());
        }
    }

    //Move basket above to make it part of the path.
    public void MoveBasketAbove()
    {
        Vector3 endPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
        StartCoroutine(MoveTo(transform, endPosition, 0.5f));
        DownBasketBarrier();
    }

    //Down basket barriers when passing level.
    public void DownBasketBarrier()
    {
        Vector3 currentFrontPos = frontBarrier.transform.localPosition;
        Vector3 endFronPosition = new Vector3(currentFrontPos.x, currentFrontPos.y - 0.337f, currentFrontPos.z);
        StartCoroutine(MoveTo(frontBarrier.transform, endFronPosition, 0.5f));

        Vector3 currentBackPos = backBarrier.transform.localPosition;
        Vector3 backFronPosition = new Vector3(currentBackPos.x, currentBackPos.y - 0.337f, currentBackPos.z);
        StartCoroutine(MoveTo(backBarrier.transform, backFronPosition, 0.5f));
    }

    //Up barriers for resetting basket in object pool.
    public void UpBasketBarrieres()
    {
        Vector3 currentFrontPos = frontBarrier.transform.localPosition;
        frontBarrier.transform.position = new Vector3(currentFrontPos.x, currentFrontPos.y + 0.337f, currentFrontPos.z);

        Vector3 currentBackPos = backBarrier.transform.localPosition;
        backBarrier.transform.position = new Vector3(currentBackPos.x, currentBackPos.y + 0.337f, currentBackPos.z);
    }

    //Move to basket above if user pass level.
    IEnumerator MoveTo(Transform mover, Vector3 destination, float speed)
    {
        yield return new WaitForSeconds(1.5f);
        while (mover.localPosition != destination)
        {
            mover.localPosition = Vector3.MoveTowards(
                mover.localPosition,
                destination,
                speed * Time.deltaTime);
            yield return null;
        }
    }

    //Change collectable color as path color when pass.
    IEnumerator CollectableAction(GameObject collectable)
    {
        yield return new WaitForSeconds(1f);
        collectable.SetActive(false);
    }
}
