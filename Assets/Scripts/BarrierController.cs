﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierController : MonoBehaviour
{
    public GameObject leftBarrier;
    public GameObject rightBarrier;

    public void OpenBarriers()
    {
        StartCoroutine(Rotate(Vector3.up, 55, 2.0f, leftBarrier.transform));
        StartCoroutine(Rotate(Vector3.up, -55, 2.0f, rightBarrier.transform));
    }

    public void CloseBarriers()
    {
        leftBarrier.transform.rotation = Quaternion.Euler(90, -90, -90);
        rightBarrier.transform.rotation = Quaternion.Euler(90, -90, -90);
    }

    //Open barriers smootly.
    IEnumerator Rotate(Vector3 axis, float angle, float duration, Transform barrier)
    {
        yield return new WaitForSeconds(.5f);
        Quaternion from = barrier.rotation;
        Quaternion to = barrier.rotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            barrier.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        barrier.rotation = to;
    }
}
