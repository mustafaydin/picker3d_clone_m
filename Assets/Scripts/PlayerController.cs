using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using TMPro;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public static bool endPath = false; //Condition to check whether collector reaches end of the path, stop it here and act according to result.
    public static bool gameStart = false; //Condition variable to wait tap from user to start game.
    public static bool passed = false; //Condition to check whether phase or level passed.
    private static bool screenSet = false; //Condition to set game screen after player tap to start.

    //Change screen according to game states.
    public GameObject screenHandler;
    private ScreenHandler screenScript;

    //Reference to level manager.
    private NewLevelManager levelManagerScript;
    public GameObject levelManager;

    public static int currentPhase = 0; //Current phase of a level.
    public static int levelCounter = 0; //Level counter to keep level index for each run to load levels correctly.
    private static Vector3 startPos; //Keep start position of picker for resetting when fail.

    private static int totalCollected = 0;
    private float powerPercentage = 0;

    public GameObject[] boostAnimation;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        screenScript = screenHandler.GetComponent<ScreenHandler>();
        levelManagerScript = levelManager.GetComponent<NewLevelManager>();
    }

    private void Start()
    {
        startPos = transform.localPosition;
    }

    public void BeginLevelEnd()
    {
        Debug.Log("End level begin");
        PlayerMovement.forwardSpeed = 9f;
        foreach (GameObject boost in boostAnimation)
        {
            boost.SetActive(true);
        }
        StartCoroutine(BoostAction());
    }


    IEnumerator BoostAction()
    {
        float fixedEndPathDuration = 3.3f;
        float timePassed = 0;
        while (timePassed < (fixedEndPathDuration * powerPercentage))
        {
            timePassed += Time.deltaTime;

            yield return null;
        }
        PlayerMovement.forwardSpeed = 0f;
        StartCoroutine(CalculateBonus());
    }

    IEnumerator CalculateBonus()
    {
        foreach (GameObject boost in boostAnimation)
        {
            boost.SetActive(false);
        }
        PlayerPrefs.SetInt("GameScore", PlayerPrefs.GetInt("GameScore") + ScoreHandler.currentScore);
        screenScript.SetGameScore();
        yield return new WaitForSeconds(2);
        PlayerMovement.forwardSpeed = 9f;
    }

    //When new level begins, reset and return gameobjects to pool and remove stored data for previous level. Also, reset phase progress according to level.
    public void LevelPassed()
    {
        Debug.Log("New level began.");
        screenScript.SetStartPhaseProgress(NewLevelManager.levelBaskets[levelCounter].Count - 1, 0);
        screenScript.SetLevelTexts();
        PlayerMovement.forwardSpeed = 3f;
        List<GameObject> baskets = NewLevelManager.levelBaskets[levelCounter - 1];
        List<GameObject> barriers = NewLevelManager.levelBarriers[levelCounter - 1];
        foreach (GameObject basket in baskets)
        {
            basket.GetComponent<BasketController>().UpBasketBarrieres();
        }
        foreach (GameObject barrier in barriers)
        {
            barrier.GetComponent<BarrierController>().CloseBarriers();
        }
        levelManagerScript.ReturnAllLevelObjects();
        levelManagerScript.ClearPassedLevelData(levelCounter - 1);
    }

    //Actions to be executed at the end of the path(level).
    public void OnChildTriggerExit(Collider other)
    {
        //Stop picker.
        rb.velocity = Vector3.zero; 
        rb.angularVelocity = Vector3.zero; 
        endPath = true;

        //Get baskets, barriers and phase requirements for current level and set score text on basket accordingly.
        List<GameObject> baskets = NewLevelManager.levelBaskets[levelCounter];
        List<GameObject> barriers = NewLevelManager.levelBarriers[levelCounter];
        string[] requirements = NewLevelManager.phaseRequirements[levelCounter];
        int.TryParse(requirements[currentPhase], out int levelReq);
        screenScript.UpdateBasketScore(CollectableController.counter, levelReq);

        //If user succeed.
        if (CollectableController.counter >= levelReq)
        {
            passed = true;

            //Count total collected item for end level power.
            totalCollected += CollectableController.counter;

            //Move basket and open barrier of current phase.
            baskets[currentPhase].GetComponent<BasketController>().MoveBasketAbove();
            barriers[currentPhase].GetComponent<BarrierController>().OpenBarriers();

            startPos = new Vector3(startPos.x, startPos.y, barriers[currentPhase].transform.position.z + 11.4f); //Update start position as next phase.
            currentPhase++; //Increment phase by 1.
            screenScript.SetStartPhaseProgress(baskets.Count - 1, currentPhase); //Set phase progress bar.

            //If it the last phase, means end of level.
            if (currentPhase == baskets.Count)
            {
                Debug.Log("Level is over!");
                int numberOfObjects = NewLevelManager.allLevelCollectables[levelCounter].Count;
                powerPercentage = ((float)totalCollected) / ((float)numberOfObjects);
                Debug.Log("Total collected: " + totalCollected + "--number of objects: " + numberOfObjects + "--perc: " + powerPercentage);
                currentPhase = 0;
                levelCounter++;
                startPos = new Vector3(startPos.x, startPos.y, NewLevelManager.endPathPosZ[levelCounter] + 14f);
                StartCoroutine(EndLevel(barriers,  baskets));
            }
            //If it is only end of phase not level.
            else
            {
                Debug.Log("Phase is over!");
                StartCoroutine(NextPhase());
            }
            int.TryParse(NewLevelManager.phaseRequirements[levelCounter][currentPhase], out int nextLevelReq);
            screenScript.SetScoreOnBasket(nextLevelReq, NewLevelManager.levelBaskets[levelCounter][currentPhase]); //Update score text on basket accordingly.
        }
        //If user failed.
        else
        {
            Debug.Log("Fail!");
            passed = false;
            StartCoroutine(Reset());
        }
        CollectableController.counter = 0;
    }

    //Move collector horizantally by touch input of user.
    void Update()
    {
        if (gameStart && !screenSet)
        {
            SetStartGameScreen();
            screenSet = true;
        }
    }

    //Set current level next level, increment next level by 1 if there is no more level pick randomly and make it next then load it.
    //Return all object to use for next levels and make arrangement on path object. Wait 3 seconds and continue to move.
    IEnumerator EndLevel(List<GameObject> resetBarriers, List<GameObject> resetBaskets)
    {
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("NextLevel", 1));
        int nextLevel = PlayerPrefs.GetInt("NextLevel", 1) + 1;
        if (nextLevel == NewLevelManager.numOfLevel)
        {
            int randomLevel = Random.Range(0, NewLevelManager.numOfLevel);
            PlayerPrefs.SetInt("NextLevel", randomLevel);
        }
        else
        {
            PlayerPrefs.SetInt("NextLevel", PlayerPrefs.GetInt("NextLevel", 1) + 1);
        }

        levelManagerScript.LoadSpecificLevel(PlayerPrefs.GetInt("NextLevel"));

        PlayerPrefs.SetInt("InfLevel", PlayerPrefs.GetInt("InfLevel", 1) + 1); //Keep track of levels without affecting game close/open.

        yield return new WaitForSeconds(3);
        endPath = false;
    }

    //Reset counter to 0 to start counting for new phase.
    IEnumerator NextPhase()
    {
        //Show success text for 3 seconds then hide it.
        //screenScript.SwitchTextActivity(ScreenHandler.TextName.Success);
        yield return new WaitForSeconds(3);
        //screenScript.SwitchTextActivity(ScreenHandler.TextName.Success);


        endPath = false; //Make end path false to continue movement of collector to next level.
        CollectableController.counter = 0; // Reset scoring for next level.
    }

    //Reset level i.e. place level objects and picker to their start positions.
    IEnumerator Reset()
    {
        yield return new WaitForSeconds(1);
        levelManagerScript.ResetCollectableObjects();

        //Show fail text for 3 seconds then hide it.
        //screenScript.SwitchTextActivity(ScreenHandler.TextName.TryAgain);
        yield return new WaitForSeconds(3);
        //screenScript.SwitchTextActivity(ScreenHandler.TextName.TryAgain);

        //Move backward collector and place it to horizantally center.
        transform.localPosition = startPos;

        PlayerController.endPath = false; //Make end path false to continue movement of collector to next level.
        CollectableController.counter = 0; // Reset scoring for try again case.
    }

    //Get rid off fade effect and tap to start text from screen when user starts game.
    void SetStartGameScreen()
    {
        screenScript.ShowAndHideFadeEffect();
        screenScript.SwitchTextActivity(ScreenHandler.TextName.TapToStart);
    }
}