﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool SharedIntance;
    private Dictionary<string, Queue<GameObject>> objectPool;

    void Awake()
    {
        SharedIntance = this;

        objectPool = new Dictionary<string, Queue<GameObject>>();
    }

    public GameObject GetObject(GameObject gameObj)
    {
        if (objectPool.TryGetValue(gameObj.name, out Queue<GameObject> objectList))
        {
            if(objectList.Count == 0)
            {
                return CreateNewObject(gameObj);
            }
            else
            {
                GameObject obj = objectList.Dequeue();
                obj.SetActive(true);
                return obj;
            }
        }
        else
        {
            return CreateNewObject(gameObj);
        }
    }

    private GameObject CreateNewObject(GameObject gameObj)
    {
        GameObject newGameObject = Instantiate(gameObj);
        newGameObject.name = gameObj.name;
        return newGameObject;
    }

    public void ReturnGameObject(GameObject gameObj)
    {
        if (objectPool.TryGetValue(gameObj.name, out Queue<GameObject> objectList))
        {
            objectList.Enqueue(gameObj);
        }
        else
        {
            Queue<GameObject> newObjectQueue = new Queue<GameObject>();
            newObjectQueue.Enqueue(gameObj);
            objectPool.Add(gameObj.name, newObjectQueue);
        }

        gameObj.transform.rotation = Quaternion.identity;
        gameObj.SetActive(false);
    }
}
