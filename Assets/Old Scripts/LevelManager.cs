using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System;


public class LevelManager : MonoBehaviour
{
    private const float pathLength = 31.97f; //Path length for a level.
    public GameObject collectablePrefab; //Collectable prefab.
    private string levelDataPath; //Level data path, Level Data folder in assets folder.
    private List<string[]> levelData = new List<string[]>(); //All level data.

    //Generate levels by extracting coordinate details from level data and place pooled object accordingly.
    void GenerateLevel(int i, string objPos, GameObject level)
    {
        string[] coordinates = objPos.Split(' ');
        foreach (string pos in coordinates)
        {
            GameObject collectable = ObjectPool.SharedIntance.GetObject(collectablePrefab);
            string[] coord = pos.Substring(1, pos.Length - 2).Split(',');
            float xCoord = float.Parse(coord[0]);
            float yCoord = float.Parse(coord[1]);
            float zCoord = float.Parse(coord[2]) + (i * pathLength);
            if (collectable != null)
            {
                collectable.transform.localPosition = new Vector3(xCoord, yCoord, zCoord);
                collectable.transform.rotation = Quaternion.identity;
                collectable.transform.parent = level.transform;
                //collectable.SetActive(true);
            }
        }
    }

    //Read level data and store it, also set postions of the gameobjects according to the starting posisitions.
    void Start()
    {
        //Read level data from data txt files.
        levelDataPath = Application.dataPath + "/Level Data";
        string[] allLevelFiles = Directory.GetFiles(levelDataPath, "*.txt");
        for (int i = 0; i < allLevelFiles.Length; i++)
        {
            string[] details = File.ReadAllLines(allLevelFiles[i]);
            levelData.Add(details);
        }

        //Generate first 2 levels from where user left off.
        GameObject level1 = new GameObject("Level " + (LevelIndex + 1).ToString());
        string objPos1 = levelData[CurrentLevelInd][1].Split(':')[1];
        GenerateLevel(LevelIndex, objPos1, level1);

        GameObject level2 = new GameObject("Level " + (LevelIndex + 2).ToString());
        string objPos2 = levelData[NextLevelInd][1].Split(':')[1];
        GenerateLevel(LevelIndex + 1, objPos2, level2);
    }

    //Delete previous level i.e. deactivate pool object to be used for upcoming levels.
    public void DeletePreviousLevel()
    {
        GameObject previousLevel = GameObject.Find("Level " + (LevelIndex).ToString());
        foreach (Transform child in previousLevel.transform)
        {
            //child.gameObject.SetActive(false);
            ObjectPool.SharedIntance.ReturnGameObject(child.gameObject);
            child.gameObject.GetComponent<CollectableController>().ResetCollectable();
        }
        previousLevel.transform.DetachChildren();
        Destroy(previousLevel);
    }

    //The function for generating next level.
    public void GenerateNextLevel(int ind)
    {
        NextLevelInd = ind;
        GameObject level = new GameObject("Level " + (LevelIndex + 2).ToString());
        string objPos = levelData[ind][1].Split(':')[1];
        GenerateLevel(LevelIndex + 1, objPos, level);
    }

    //Reset a level means that reset all position according to level data.
    public void ResetLevel(int levelInd, int currentId)
    {
        GameObject resetLevel = GameObject.Find("Level " + (levelInd + 1).ToString());
        string objPos = levelData[currentId][1].Split(':')[1];
        string[] pos = objPos.Split(' ');
        for (int i = 0; i < pos.Length; i++)
        {
            Transform collectable = resetLevel.transform.GetChild(i);
            collectable.gameObject.SetActive(false);
            collectable.GetComponent<CollectableController>().ResetCollectable();//Change the movement status of collectable object, i.e. stop effect of add force at the end of the level.

            //Extract coordinate data.
            string[] coord = pos[i].Substring(1, pos[i].Length - 2).Split(',');
            float xCoord = float.Parse(coord[0]);
            float yCoord = float.Parse(coord[1]);
            float zCoord = float.Parse(coord[2]) + (levelInd * pathLength);

            //Set the positions.
            collectable.transform.position = new Vector3(xCoord, yCoord, zCoord);
            collectable.transform.rotation = Quaternion.identity;

            collectable.gameObject.SetActive(true);
        }
    }

    public int GetLevelRequirements(int levelId)
    {
        string objCount = levelData[levelId][0].Split(':')[1];
        int numOfObjs = Int32.Parse(objCount);
        return numOfObjs;
    }

    /// HELPERS, GETTER SETTERS ///

    //Level number (1 to inf)
    private int LevelIndex
    {
        get { return PlayerPrefs.GetInt("CurrentLevel", 1) - 1; }
    }

    //Current level index (0 to 9 since we have 10 different levels)
    private int CurrentLevelInd
    {
        get { return PlayerPrefs.GetInt("Current", 0); }
        set
        {
            PlayerPrefs.SetInt("Current", value);
        }
    }

    //Net level index since we create 2 levels at the start. (0 to 9 since we have 10 different levels)
    private int NextLevelInd
    {
        get { return PlayerPrefs.GetInt("Next", 1); }
        set
        {
            PlayerPrefs.SetInt("Next", value);
        }
    }
}