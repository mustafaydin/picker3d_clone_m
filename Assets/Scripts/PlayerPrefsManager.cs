﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefsManager
{
    public static int CurrentLevel
    {
        get
        {
            return PlayerPrefs.GetInt("CurrentLevel", 0);
        }
        set
        {
            PlayerPrefs.SetInt("CurrentLevel", value);
        }
    }

    public static int NextLevel
    {
        get
        {
            return PlayerPrefs.GetInt("NextLevel", 1);
        }
        set
        {
            PlayerPrefs.SetInt("NextLevel", value);
        }
    }

    public static int InfLevel
    {
        get
        {
            return PlayerPrefs.GetInt("InfLevel", 1);
        }
        set
        {
            PlayerPrefs.SetInt("InfLevel", value);
        }
    }

    public static float LastZ
    {
        get
        {
            return PlayerPrefs.GetFloat("LastZ");
        }
        set
        {
            PlayerPrefs.SetFloat("LastZ", value);
        }
    }

    public static float LastObjZ
    {
        get
        {
            return PlayerPrefs.GetFloat("LastObjZ");
        }
        set
        {
            PlayerPrefs.SetFloat("LastObjZ", value);
        }
    }
}
