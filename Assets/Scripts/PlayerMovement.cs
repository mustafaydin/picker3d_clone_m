﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody rb;
    private SwerveInputManager swerveInputManager;
    private float horizantalSpeed = 10f;
    public static float forwardSpeed = 3f;
    public static float xVal;

    void Awake()
    {
        swerveInputManager = GetComponent<SwerveInputManager>();
        rb = GetComponent<Rigidbody>();
    }

    //Keep picker's y position constant.
    private void Update()
    {
        //transform.position = new Vector3(transform.position.x, -2.417f, transform.position.z);
        transform.position = new Vector3(transform.position.x, -1.77f, transform.position.z);
    }

    //Move character forward and horizantally according to input calculation as Delta X.
    void FixedUpdate()
    {
        float swerveDis = Time.deltaTime * horizantalSpeed * swerveInputManager.DeltaX();
        swerveDis = Mathf.Clamp(swerveDis, -1.35f, 1.35f);
        if (!PlayerController.endPath && PlayerController.gameStart)
        {
            rb.velocity = new Vector3(swerveDis * 3f, 0, forwardSpeed);
        }
    }
}
