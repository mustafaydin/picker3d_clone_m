﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScreenHandler : MonoBehaviour
{
    public enum TextName
    {
        Success,
        TryAgain,
        TapToStart,
    }

    //Text references to show level result at the end of each level.
    public TextMeshProUGUI successText;
    public TextMeshProUGUI tryAgainText;
    public TextMeshProUGUI tapToStartText;
    public TextMeshProUGUI scoreOnBasketText;

    public Text currentLevel;
    public Text nextLevel;

    //Image to have fade screen effect.
    public Image fadeScreen;

    //Score related variables.
    private static string scoreText;

    //Phase progress bars.

    public Image phaseProgress;

    public Sprite[] onePhase = new Sprite[2];
    public Sprite[] twoPhase = new Sprite[3];
    public Sprite[] threePhase = new Sprite[4];

    //Game score.
    public Text gameScore;

    public void SetGameScore()
    {
        int score = PlayerPrefs.GetInt("GameScore", 0);
        gameScore.text = score.ToString();
    }

    public void SetLevelTexts()
    {
        int currentLevelNum = PlayerPrefs.GetInt("InfLevel", 1);
        currentLevel.text = currentLevelNum.ToString();
        nextLevel.text = (currentLevelNum + 1).ToString();
    }

    private List<Sprite[]> allPhaseTypes;

    private void Start()
    {
        allPhaseTypes = new List<Sprite[]>();
        allPhaseTypes.Add(onePhase);
        allPhaseTypes.Add(twoPhase);
        allPhaseTypes.Add(threePhase);
        SetLevelTexts();
        SetGameScore();
    }

    public void SetStartPhaseProgress(int phaseInd, int currentPhase)
    {
        phaseProgress.sprite = allPhaseTypes[phaseInd][currentPhase];
    }

    //Update score text according to scores.
    private void SetBasketText(int collected, int required)
    {
        scoreText = String.Format("{0}/{1}", collected.ToString(), required.ToString());
    }

    //Reset position of basket and reset scoring.
    public void SetScoreOnBasket(int reqs, GameObject basket)
    {
        StartCoroutine(SetBasketScore(reqs, basket));
    }

    IEnumerator SetBasketScore(int reqs, GameObject basket)
    {
        yield return new WaitForSeconds(1f);
        Vector3 basketScorePosition = scoreOnBasketText.transform.position;
        scoreOnBasketText.transform.position = new Vector3(basketScorePosition.x, basketScorePosition.y, basket.transform.position.z - 1);
        SetBasketText(0, reqs);
        scoreOnBasketText.text = scoreText;
    }

    //Update basket score according to collected number of items.
    public void UpdateBasketScore(int totalCollected, int required)
    {
        SetBasketText(totalCollected, required);
        StartCoroutine(UpdateScore());
    }

    IEnumerator UpdateScore()
    {
        yield return new WaitForSeconds(.5f);
        scoreOnBasketText.text = scoreText;
    }

    public void ActivitySwitcher(GameObject text)
    {
        if (text.activeSelf)
        {
            text.SetActive(false);
        }
        else
        {
            text.SetActive(true);
        }
    }

    //Activate or deactivate texts.
    public void SwitchTextActivity(TextName text)
    {
        switch (text)
        {
            case TextName.Success:
                ActivitySwitcher(successText.gameObject);
                break;

            case TextName.TryAgain:
                ActivitySwitcher(tryAgainText.gameObject);
                break;

            case TextName.TapToStart:
                ActivitySwitcher(tapToStartText.gameObject);
                break;
        }
    }

    //Show and hide fade effect.
    public void ShowAndHideFadeEffect()
    {
        ActivitySwitcher(fadeScreen.gameObject);
    }
}
