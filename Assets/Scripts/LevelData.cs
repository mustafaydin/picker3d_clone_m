﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelData
{
    public string levelName; //Level name.
    public List<Vector3> pathCoordinates; //Coordinates of path related objects: tile, basket, and end level to place them.
    public List<String> pathTypes; //Type of path objects to place them.
    public List<Vector3> allScoreCoordinates;
    public List<int> allScores;
    public List<Vector3> objectCoordinates; //Collectable object coordinates.
    public List<String> objectTypes; //Types of collectable object coordinates (sphere, cube, cylinder).
    public string [] phaseRequirements; //Level requirements phase by phase.
}